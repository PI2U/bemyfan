package queries;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import util.config;

@ManagedBean(name = "database")
@SessionScoped
public class Database {


	private  static Connection conn = null;
	private  static  Database db = null;
	private Statement stmt = null;
	
	public static synchronized Database getInstance()
	{
		if (db == null)
			db = new Database();
		return db;
	}

	public Database() {
		
		if (db == null){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(config.dbUrl, config.user, config.pass);
			System.out.println("Connected to Database!");
		} catch (Exception e) {
			db = null;
			System.err.println("Error connecting to Database!");
			//e.printStackTrace();
		
		}	
		}
	}

	public ResultSet query(String statement) {
		
		ResultSet rs;
		
	
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(statement);

			return rs;
		} catch (SQLException e) {
			System.err.println("Error in Query");
			e.printStackTrace();
		}

		return null;
	}

	public boolean insert(String statement) {
		
		try {
			
			stmt = conn.createStatement();
			
			stmt.execute(statement);
			return true;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public List<String> queryString(String statement) {
		ResultSet rs;
		List<String> out = new ArrayList<String>();
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(statement);

			while (rs.next()) {				
				int i = 2;
				String tmp = rs.getString(i-1) + ";#;";
				for (; i <= rs.getMetaData().getColumnCount() -1 ; i++) {
					tmp += rs.getString(i) + ";#;";
				}
				tmp += rs.getString(i );
				out.add(tmp);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error in der Datenbankverbindung");
			e.printStackTrace();
		}

		return out;
	}

	public boolean queryNotNull(String query){
		
		ResultSet rs = null;
		rs = query(query);
		
		try {
			if(rs != null && rs.first()){
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return false;
	}
	
	public boolean is_status(int userID, int statusID) {

		ResultSet rs = null;
		
			rs = query(Sql.select + Sql.user_status + " and user.id = "
					+ userID);

			try {
				rs.next();
				if (rs.getInt(4) == statusID) {
					return true;
				}

			} catch (SQLException e) {
				System.out.println("User hat keinen Status!");
			}
		
		return false;
	}

	
	
	public List<String> getStatusList() {

		return queryString(Sql.select + Sql.user_status);
	}
}
