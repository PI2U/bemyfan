package chat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.*;

import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;



@ManagedBean(name = "chat")
@ApplicationScoped
public class Chat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3097033376159998878L;
	private String text;
	private List<String> history;


	public Chat() {
		history = new ArrayList<String>();
		
		
	}

	public String clear(){
		history.clear();
		return "";
	}
	
	public String getText() {
		return text;
	}

	public synchronized void  setText(String text) {

			
			history.add(getNow() + " : " + text + "\n");
			
	        PushContext pushContext = PushContextFactory.getDefault().getPushContext();  
	        pushContext.push("/chat", getNow() + " : " + text + "\n"); 
			
	}
	
    private int count;  
    public synchronized void increment() {
    	count++;
    	PushContext pushContext = PushContextFactory.getDefault().getPushContext();
    	pushContext.push("/chat", String.valueOf(count));
    	}
    
    public int getCount() {  
        return count;  
    }  
  
    public void setCount(int count) {  
        this.count = count;  
    }  
    
	public  List<String> getHistory() {

		
		return history;

	}

	public void setHistory(List<String> history) {
		this.history = history;
	}

	public Date getNow() {
		return (new Date());
	}


	public void setCount(Integer count) {
		this.count = count;
	}
}
