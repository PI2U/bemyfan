package chat;

import java.util.LinkedList;

public class ChatUsers {

	private LinkedList<User> users;
	
	public ChatUsers(){
		users = new LinkedList<User>();
	}
	
	public synchronized void addUser(String usr){
		users.add(new User(usr));
	}
	
	public synchronized void removeUser(String usr){
		for(User e: users){
			if(e.getName().equalsIgnoreCase(usr)){
				users.remove(e);
			}
		}
	}	
	
	public synchronized boolean contains(String usr){
		for(User e: users){
			if(e.getName().equalsIgnoreCase(usr)){
				return true;
			}
		}
		return false;
	}
}
