package chat;

import javax.faces.application.FacesMessage;  
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;  
import javax.faces.event.ActionEvent;  

import org.primefaces.push.PushContext;  
import org.primefaces.push.PushContextFactory;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
  
import javax.faces.application.FacesMessage;  
import javax.faces.context.FacesContext;  
  
import org.primefaces.event.SelectEvent;
@ManagedBean(name="growlBean")
public class GrowlBean {  
  
    private String summary;  
      
    private String detail;  
      
    public String getDetail() {  
        return detail;  
    }  
  
    public void setDetail(String detail) {  
        this.detail = detail;  
    }  
    private Date date1;  
    
    public Date getDate1() {  
        return date1;  
    }  
  
    public void setDate1(Date date1) {  
        this.date1 = date1;  
    }  
  
    public void handleDateSelect(SelectEvent event) {  
        FacesContext facesContext = FacesContext.getCurrentInstance();  
        SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");  
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));  
    }  
    public String getSummary() {  
        return summary;  
    }  
    public void setSummary(String summary) {  
        this.summary = summary;  
    }  
  
    public void save(ActionEvent actionEvent) {  
        FacesContext context = FacesContext.getCurrentInstance();  
          
        context.addMessage(null, new FacesMessage("Successful", "Hello " + "joejooejjo"));  
        context.addMessage(null, new FacesMessage("Second Message", "Additional Info Here..."));  
    }  
      
    public void send() {  
        PushContext pushContext = PushContextFactory.getDefault().getPushContext();  
          
        pushContext.push("/notifications", new FacesMessage(summary, detail));  
    }  
}  
                      
