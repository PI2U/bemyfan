package user;

import java.io.File;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import queries.Database;
import queries.Sql;
import util.crypto;
import util.osdetect;

import java.util.Random;
import java.util.UUID;



@ManagedBean(name = "adduser")
@SessionScoped
public class adduser {
	
	private String name;
	private String vorname;
	private String email;
	private String date;
	private String land;
	private String passwort;



	public boolean newuser(String name, String vorname, String email,
			String date, String land, int status, String passwort, char geschlecht) {
		String guid = UUID.nameUUIDFromBytes((new Random().nextLong() + name+ vorname + email + date + land + crypto.SHA512(passwort)).getBytes()).toString();
		
		String query = Sql.add_user
				+ "("
				+ 0
				+ ",\""
				+ name
				+ "\",\""
				+ vorname
				+ "\",\""
				+ email
				+ "\",\""
				+ date
				+ "\",\""
				+ land
				+ "\","
				+ 0
				+ ","
				+ status
				+ ",\""
				+ passwort
				+ "\",\""
				+ guid + "\",\""
				+ geschlecht
				+ "\")";

	
		if (checkuser(email) ) {
			if(Database.getInstance().insert(query)){


				
				File f = new File(osdetect.getUploadPath() + guid );
				boolean success = f.mkdirs();
				if (!success) {
				    System.out.println("Error creating dir!" + f.getAbsolutePath());
				}else{
					
				}
				return true;
			}
		}

		return false;
	}

	public boolean checkuser(String email) {

		String query = Sql.select + Sql.user_login_email + email + "\"";


		if (Database.getInstance().queryNotNull(query)) {

			return false;
		}

		return true;
	}

	
	public void validateUser(FacesContext context, UIComponent toValidate, Object value) throws ValidatorException {
        
       String email = (String) value;
        
       if(!email.contains("@")) {
           FacesMessage message = new FacesMessage();
           message.setSeverity(FacesMessage.SEVERITY_ERROR);
           message.setSummary("Email is not valid.");
           message.setDetail("Email is not valid.");
           context.addMessage("userForm:Email", message);
           throw new ValidatorException(message);
       }
       
       if(!checkuser(email)) {
           FacesMessage message = new FacesMessage();
           message.setSeverity(FacesMessage.SEVERITY_ERROR);
           message.setSummary("User already exists!");
           message.setDetail("User already exists!");
           context.addMessage("userForm:Email", message);
           throw new ValidatorException(message);
       }
       
       
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(checkuser(email)){
			this.email = email;
		}
		
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

}
