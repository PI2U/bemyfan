package user;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import java.util.UUID;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;

import queries.Database;
import queries.Sql;
import util.ImageResize;
import util.config;
import util.crypto;
import util.osdetect;

@ManagedBean(name = "loginHandler")
@SessionScoped
public class loginHandler {

	private String email = null;
	private String pass = null;
	private String vorname = null;
	private String nachname = null;
	private String guid = null;
	private String land = null;
	private char geschlecht = 'm';
	private int kontostand = 0;
	private int status = 0;
	private int uid = 0;
	private Date geburtstag = new Date();
	private Part file = null;
	private ExternalContext ex;
	private boolean loggedin = false;
	List<String> piclist = new ArrayList<String>();
	SimpleDateFormat df = new SimpleDateFormat( "dd.MM.yyyy" );
	private String  profilbild;
	
	public loginHandler() {
		
	}

	public String login() {

		String query = Sql.select + Sql.user_login_email + email
				+ Sql.user_login_pass + pass + "\"";
		System.out.println(query);
		String userinfo;
		try {
			
			userinfo = Database.getInstance().queryString(query).get(0);

		} catch (Exception e) {
			System.err.println("Daten inkorrekt!");
			
			return "error.jsf";
		}

		loggedin = true;
		pass = null;
		setUserinfo(userinfo);

		try {
			ResultSet rs = Database.getInstance().query(
					Sql.select + Sql.get_pics + uid);

			while (rs.next()) {
				piclist.add(rs.getString("pictureid") + "_small");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ex = javax.faces.context.FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			ex.redirect("index.jsf");
		} catch (Exception e) {
			// TODO Auto-generated catch block

		}
		return "index.jsf";
	}

	public String newUser() {

		if (!loggedin) {
			adduser tmp = new adduser();

			if (tmp.newuser(nachname, vorname, email, df.format(geburtstag),
					land, status, pass, geschlecht)) {
				login();
				return "success.jsf";
			}
		}
		return "index.jsf";

	}

	public String getData() {
		return vorname + " " + nachname + " " + geburtstag + " " + land + " "
				+ kontostand + " " + status + " " + uid + " " + guid;
	}

	private void setUserinfo(String userinfo) {

		if (userinfo.length() > 2) {
			
			String[] tmp = userinfo.split(";#;");
			uid = Integer.parseInt(tmp[0]);
			nachname = tmp[1];
			vorname = tmp[2];
			try {
				geburtstag = df.parse( tmp[4] );
			} catch (ParseException e) {
				
				e.printStackTrace();
			}
			land = tmp[5];
			kontostand = Integer.parseInt(tmp[6]);
			status = Integer.parseInt(tmp[7]);
			guid = tmp[9];
		}
			String tmp = Sql.select + Sql.get_profile_pics + "\"" + uid + "\"";
			try{
			tmp = Database.getInstance().queryString(tmp).get(0);
			}
			catch(Exception e){
				System.out.println(tmp);
				profilbild = "profil.png";
			}

	}

	public void update(){
		String query = Sql.update_user + 
				"Name" + "=\"" + nachname + "\", " +
				"Vorname" + "=\"" + vorname + "\", " +
				"Geburtstag" + "=\"" + df.format(geburtstag) + "\", " +
				"Land" + "=\"" + land + "\", " +
				"Geschlecht" + "=\"" + geschlecht + "\"";
		
		Database.getInstance().insert(query);
		
	}
	
	public String logout() {
		uid = 0;
		nachname = null;
		vorname = null;
		geburtstag = null;
		land = null;
		kontostand = 0;
		status = 0;
		guid = null;
		loggedin = false;
		email = null;
		geschlecht = ' ';
		ex = javax.faces.context.FacesContext.getCurrentInstance()
				.getExternalContext();
		ex.invalidateSession();
		
		return "index.jsf";
	}

	public List<String> getImages() {

		return piclist;
	}

	public void checkcaptcha(ActionEvent event) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Correct", "Correct");

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public String upload() {

		byte[] buf = new byte[(int) (file.getSize())];

		try {

			file.getInputStream().read(buf);
			UUID pic_uuid = UUID
					.nameUUIDFromBytes((file.getSubmittedFileName() + (new Random())
							.nextLong()).getBytes());
			String Path = osdetect.getUploadPath() + guid + osdetect.getSeperator() + pic_uuid;
			
			FileOutputStream out = new FileOutputStream(Path);
					
			out.write(buf);
			out.close();

			ImageResize.resize(new File(Path), Path + "_small");
			
			String query = Sql.add_pic + "(" + uid + ", \"" + pic_uuid + "\")";
			Database.getInstance().insert(query);
			piclist.add(pic_uuid.toString() + "_small");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (file != null) {
			FacesMessage msg = new FacesMessage("Succesful",
					file.getSubmittedFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		try {
			ex = javax.faces.context.FacesContext.getCurrentInstance()
					.getExternalContext();
			ex.redirect("index.jsf");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "index.jsf";
		}

		return "index.jsf";
	}

	public void validateFile(FacesContext ctx, UIComponent comp, Object value) {

		Part file = (Part) value;
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		message.setSummary("Cant upload this file!");


		if (file.getSize() > 1024 * 1024 * 5) {
			ctx.addMessage("userForm:file", message);
			throw new ValidatorException(message);
		}
		if (!(file.getContentType().startsWith("image/"))) {
			ctx.addMessage("userForm:file", message);
			throw new ValidatorException(message);
		}

	}

	public Part getFile() {

		return file;
	}

	public void setFile(Part file) {

		this.file = file;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String passwort) {
		this.pass = crypto.SHA512(passwort);

	}

	public boolean isLoggedin() {

		return loggedin;
	}

	public void setLoggedin(boolean logged_in) {
		this.loggedin = logged_in;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public int getKontostand() {
		return kontostand;
	}

	public void setKontostand(int kontostand) {
		this.kontostand = kontostand;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public Date getGeburtstag() {
		return geburtstag;
	}

	public void setGeburtstag(Date geburtstag) {
		this.geburtstag = geburtstag;

	}


	
	public char getGeschlecht() {
		return geschlecht;
	}

	public void setGeschlecht(char geschlecht) {
		this.geschlecht = geschlecht;
	}

	public String getProfilbild() {
		
		return profilbild;
	}

	public void setProfilbild(String profilbild) {
		this.profilbild = profilbild;
	}

}
