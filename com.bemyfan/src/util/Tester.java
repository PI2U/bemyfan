package util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import queries.Database;
import queries.Sql;
import user.adduser;

public class Tester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
			
			System.out.println("Executing Query: " + Sql.select + Sql.user_status);
			//List<String> out = Database.getInstance().queryString(Sql.select+ Sql.all_user);
					
			//for(String e: out){
			//		System.out.println(e);
			//}
		
		
		System.out.println("Executing is_status(1,1): " + Database.getInstance().is_status(1,1));
		System.out.println("Executing newuser(*): " );
		adduser usr = new adduser();
		
		usr.newuser("Wessels","Oliver","john.rambo93@gmx.net", "20.10.1989" ,"Deutschland",0,crypto.SHA512("test123"),'m');
		usr.newuser("Wessels","Patrick","patrick.wessels@gmx.net", "26.10.1989" ,"Deutschland",1,crypto.SHA512("test123"),'m');
		SimpleDateFormat df = new SimpleDateFormat( "dd.MM.yyyy" );
		Random rnd = new Random();
		
		for(int i = 0; i< 20;i++){
			if(i % 10000 == 0){
				System.out.println(i);
			}
			
			usr.newuser("Wessels","Oliver","john.rambo" + rnd.nextInt()  + "@gmx.net", df.format(new Date(rnd.nextLong())) ,"Deutschland",rnd.nextInt(5),crypto.SHA512("test123"),'w');
		}


		
	}

}
