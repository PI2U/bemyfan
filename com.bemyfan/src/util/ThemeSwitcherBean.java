package util;

import java.util.ArrayList;
import java.util.List;  
import java.util.Map;  
import java.util.TreeMap;  

import javax.annotation.PostConstruct;  
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
  
@ManagedBean(name="themeSwitcherBean") 
@SessionScoped
public class ThemeSwitcherBean {  
          
    private List<String> themes;  
       
      
    private String theme = "afternoon";  
       
 
      
    public List<String> getThemes() {  
        return themes;  
    }  
  
    public String getTheme() {  
        return theme;  
    }  
  
    public void setTheme(String theme) {  
        this.theme = theme;  
    }  
  
    @PostConstruct  
    public void init() {  
          
          
        themes = new ArrayList<String>();  
 
        themes.add("afterdark");  
        themes.add("afternoon");  
        themes.add("afterwork");     
    }  
 
  

} 
