package util;

public final class osdetect {
	private static String OS = null;
	private static String Seperator = null;

	public static String getOsName() {
		if (OS == null) {
			OS = System.getProperty("os.name");
		}

		return OS;
	}

	public static boolean isWindows() {
		return getOsName().startsWith("Windows");
	}

	public static boolean isUnix() {
		return getOsName().startsWith("Linux");
	}

	public static String getSeperator() {
		if (OS != null) {
			if (isWindows()) {
				Seperator = "\\\\";
			} else if (isUnix()) {
				Seperator = "////";
			} else {
				Seperator = "////";
			}
		}
		return Seperator;
	}
	
	public static String getUploadPath(){
		
		if(isWindows()){
			return util.config.UPLOAD_PATH_WIN;
		}
		
		return util.config.UPLOAD_PATH_LINUX;
	}

}