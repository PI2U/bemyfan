package util;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;




@ManagedBean(name="tabs")
@SessionScoped

public class Tabs {

	public String current = "current";
	
	public Tabs(){
		
		
	}

	
	public List<String> getTabs() {

		List<String> out = new ArrayList<String>();
		
		for(int i= 0;i<10;i++){
			out.add("Tab"+i);
		}
		
		return out;
	}
	
	public String getCurrent(String jo,String param){
		

		if(param.isEmpty()){
			param = this.current;
		}

		
		if(jo.equals(param)){
			this.current = jo;
			param = null;
			return "current";
		}
		return null;
	}
	
	public String getCurrent(){

			return current;

	}
}
