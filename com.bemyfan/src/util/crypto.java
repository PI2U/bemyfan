package util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class crypto {

    private static String convertToHex(byte[] data)
    {
    	BigInteger number = new BigInteger(1,data);
    	return number.toString(16);
    }
 
    public static String SHA512(String text){
    try{
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-512");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("UTF-8"), 0, text.length());
        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }catch( Exception e){
    	
    }
    return null;
    }
}
