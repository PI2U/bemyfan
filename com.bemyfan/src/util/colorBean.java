package util;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "colorBean")
@ApplicationScoped
public class colorBean {  
	  
    private String color = "1c8eff";  
    private String color2 = "ffffff"; 
  
    public String getColor() {  
        return color;  
    }  
    
    public String getColor2() {  
        return color2;  
    } 
    
    public void setColor(String color) { 
    	
        this.color = color;  
    }  
    
    public void setColor2(String color2) { 
    	
        this.color2 = color2;  
    } 
    
    public int getColorR() {  
        return Integer.decode( "0x" + color.substring(0, 2) );  
    }   
  
    public int getColorG() {  
    	return Integer.decode( "0x" + color.substring(2, 4) ); 
    }  
    
    public int getColorB() {  
    	return Integer.decode( "0x" + color.substring(4, 6) ); 
    }  
    
    public int getColor2R() {  
        return Integer.decode( "0x" + color2.substring(0, 2) );  
    }   
  
    public int getColor2G() {  
    	return Integer.decode( "0x" + color2.substring(2, 4) ); 
    }  
    
    public int getColor2B() {  
    	return Integer.decode( "0x" + color2.substring(4, 6) ); 
    } 
}  
