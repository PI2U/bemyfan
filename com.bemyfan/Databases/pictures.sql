/*
Navicat MySQL Data Transfer

Source Server         : server
Source Server Version : 50167
Source Host           : 192.168.0.17:3306
Source Database       : user

Target Server Type    : MYSQL
Target Server Version : 50167
File Encoding         : 65001

Date: 2013-08-05 23:41:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pictures
-- ----------------------------
DROP TABLE IF EXISTS `pictures`;
CREATE TABLE `pictures` (
  `userid` int(11) NOT NULL,
  `pictureid` varchar(50) NOT NULL,
  PRIMARY KEY (`userid`,`pictureid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
