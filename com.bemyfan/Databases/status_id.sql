/*
Navicat MySQL Data Transfer

Source Server         : server
Source Server Version : 50167
Source Host           : 192.168.0.17:3306
Source Database       : user

Target Server Type    : MYSQL
Target Server Version : 50167
File Encoding         : 65001

Date: 2013-08-05 23:41:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for status_id
-- ----------------------------
DROP TABLE IF EXISTS `status_id`;
CREATE TABLE `status_id` (
  `status_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
