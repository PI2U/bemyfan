/*
Navicat MySQL Data Transfer

Source Server         : server
Source Server Version : 50167
Source Host           : 192.168.0.17:3306
Source Database       : user

Target Server Type    : MYSQL
Target Server Version : 50167
File Encoding         : 65001

Date: 2013-08-05 23:41:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Vorname` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Geburtstag` varchar(33) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Land` varchar(100) DEFAULT NULL,
  `Kontostand` bigint(100) DEFAULT NULL,
  `Status` tinyint(6) DEFAULT NULL,
  `Passwort` varchar(128) DEFAULT NULL,
  `GUID` char(36) DEFAULT NULL,
  `Geschlecht` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
